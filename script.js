// liste des couleurs
const colors = ['yellow', 'orange', 'blue', 'green', 'purple', 'red', 'teal', 'black'];

// liste des cartes
let cards = [];

// on remplit la liste des cartes
for (let i = 0; i < colors.length; i++) {
    cards.splice(Math.floor(Math.random() * colors.length), 0, colors[i]);
    cards.splice(Math.floor(Math.random() * colors.length), 0, colors[i]);
}

// on récupère la balise ayant l'id page dans une variable
let page = document.getElementById('page');

// liste des cartes retournés (liste des couleurs des cartes)
let visibleCards = [];

// score (ps: on gagne un point quand on trouve une paire)
let score = 0;

// on créer les cartes et on les ajoute au body
for (let i = 0; i < cards.length; i++) {
    let card = document.createElement('div');
    card.className = 'card';
    card.setAttribute('data-color', cards[i]);
    card.addEventListener('click', clickOnCard);
    page.appendChild(card);
}

// ce qui se passe quand on clique sur une carte
function clickOnCard() {
    // on vérifie que la carte cliquée n'est pas retournée, on vérifie également qu'il n'y a pas plus de deux cartes retournées
    if (visibleCards.length !== 2 && (this.style.backgroundColor === 'grey' || this.style.backgroundColor === '')) {
        // on récupère la couleur de la carte qu'on retourne
        this.style.backgroundColor = this.getAttribute('data-color');
        // on ajoute la couleur dans le tableau de carte retournées
        visibleCards.push(this.getAttribute('data-color'));
        // si deux cartes sont retournées :
        if (visibleCards.length === 2) {
            // on attent 1 seconde puis ...
            setTimeout(function () {
                let cs = document.getElementsByClassName('card');
                if (visibleCards[0] !== visibleCards[1]) {
                    // si les deux cartes retournées sont différentes ont les remet face cachés
                    // (ici en fait on remet face cachés toutes les cartes qui n'ont pas été trouvés en paires)
                    for (let j = 0; j < cs.length; j++) {

                        if (cs[j].getAttribute('data-done') !== "1") {
                            cs[j].style.backgroundColor = 'grey';
                        }
                    }
                } else {
                    // si les deux cartes retournées sont identiques on les marque comme trouvés
                    // (ici en fait on remet face cachés toutes les cartes qui n'ont pas été trouvés en paires)
                    for (let k = 0; k < cs.length; k++) {
                        if (cs[k].style.backgroundColor !== 'grey') {
                            cs[k].setAttribute('data-done', "1");
                        }
                    }
                    // on augmente le score de 1 point
                    score++;
                    // si le score max est atteint la partie est terminée
                    if (score === cards.length / 2) {
                        alert("c'est terminé !");
                    }
                }
                // dans les deux cas on remet la liste des cartes retournés à 0
                visibleCards = [];
            }, 1000)
        }
    }
}
